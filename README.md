# Changelog ICTShop Accounting

* Added   : untuk penambahan fitur.
* Changed : untuk perubahan pada menu yang sudah ada atau optimasi.
* Removed : untuk menghapus fitur.
* Fixed   : untuk perbaikan bug atau error.

# [3.3.0.10] - 2019-12-03
## Menu Import Barang Lengkap
- **Fixed :**
	1. Fix kesalahan penulisan keterangan `IMPORT OBAT` diganti `IMPORT BARANG`.
## Menu Laporan Laba Rugi
- **Fixed :**
	1. Fix informasi periode tidak tampil.

# [3.3.0.9] - 2019-07-31
## Menu Retur Pembelian Barang
- **Fixed :**
	1. Fix salah posting jurnal keuangan menyebabkan laporan neraca tidak balance.

# [3.3.0.8] - 2018-11-02
## Menu Retur Pembelian Barang
- **Added :**
	1. Cetak faktur kecil versi 58 dan 76.
## Menu Retur Penjualan Barang
- **Added :**
	1. Cetak faktur kecil versi 58 dan 76.
## Menu Pembayaran Piutang
- **Added :**
	1. Cetak faktur kecil versi 58 dan 76.

# [3.3.0.7] - 2018-02-08
## Menu Retur Pembelian Barang
- **Added :**
	1. Cetak faktur retur menggunakan sistem load data.
- **Changed :**
	1. Optimasi menu.
	2. Keterangan transaksi jurnal dilengkapi nama supplier dan jenis pembayaran.
- **Fixed :**
	1. Fix error `invalid class typecast`, pada saat mengisi jumlah barang yang akan diretur.

# [3.3.0.6] - 2018-02-05
## Menu Penjualan Barang 
- **Changed :**
	1. Keterangan transaksi jurnal dilengkapi nama pembeli dan jenis pembayaran.
## Menu Retur Penjualan Barang
- **Added :**
	1. Cetak faktur retur menggunakan sistem load data.
- **Changed :**
	1. Optimasi menu.
	2. Keterangan transaksi jurnal dilengkapi nama pembeli dan jenis pembayaran.
- **Fixed :**
	1. Fix selisih akun piutang di laporan neraca keuangan ketika ada retur penjualan hutang. 
## Menu Pembayaran Piutang
- **Changed :**
	1. Keterangan transaksi jurnal dilengkapi nama pembeli dan jenis pembayaran.

# [3.3.0.5] - 2017-12-06
## Menu Ganti Shift
- **Changed**
	1. Tambah informasi pembayaran uang muka penjualan barang hutang sebagai pemasukan di menu dan hasil cetak tutup shift.
## Menu Penjualan Barang
- **Changed**
	1. Jumlah bayar otomatis terisi sesuai jumlah grandtotal ketika dipilih jenis penjualan bank.
	2. Validasi jumlah bayar tidak boleh kurang dari jumlah grandtotal untuk jenis penjualan tunai dan bank.
	3. Hasil cetak struk 58mm dan 76mm muncul informasi jumlah uang muka dan sisa bayar ketika penjualan hutang.
## Menu Pembayaran Piutang
- **Changed**
	1. Update informasi data piutang diurutkan berdasarkan no faktur penjualan.
	2. Update format hasil cetak faktur pembayaran piutang.
## Menu Laporan Pembayaran Piutang
- **Changed**
	1. Tambah informasi nomor faktur penjualan di tabel dan hasil cetak laporan.
## Menu Laporan Neraca 
- **Fixed**
	1. Fix pembayaran uang muka penjualan hutang belum terhitung.
